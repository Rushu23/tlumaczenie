let myImage = document.querySelector('img');

myImage.onclick = function() {
    let mySrc = myImage.getAttribute('src');
    if(mySrc === 'images/firefox.png') {
      myImage.setAttribute ('src','images/firefox2.jpg');
    } else {
      myImage.setAttribute ('src','images/firefox.png');
    }
}
let myButton = document.querySelector('button');
let myHeading = document.querySelector('h1');

function setUserName() {
    let myName = prompt('Podaj swoje imie.');
    localStorage.setItem('name', myName);
    myHeading.textContent = 'Witaj, ' + myName;
  }

if(!localStorage.getItem('name')) {
    setUserName();
  } else {
    let storedName = localStorage.getItem('name');
    myHeading.textContent = 'Witaj, ' + storedName;
  }

myButton.onclick = function() {
    setUserName();
  }

let tlumaczenie = {
  pl:{
    b1:"W Mozilli jesteśmy globalną społecznością",
    b2:"technolodzy",
    b3:"myśliciele",
    b4:"tworzyciele",
    b5:"współpracując, aby Internet był żywy i dostępny, aby ludzie na całym świecie mogli być informowanymi współtwórcami i twórcami sieci. Wierzymy, że ten akt ludzkiej współpracy na otwartej platformie ma zasadnicze znaczenie dla indywidualnego rozwoju i naszej zbiorowej przyszłości.",
    b6:"Przeczytaj",
    b7:"Manifest Mozilli",
    b8:"aby dowiedzieć się jeszcze więcej o wartościach i zasadach, które kierują naszą misją.",
    imie:"Podaj swoje imie",
  },
  en: {
    b1:"At Mozilla, we're a global community of",
    b2:"technologists",
    b3:"thinkers",
    b4:"builders",
    b5:"working together to keep the Internet alive and accessible, so people worldwide can be informed contributors and creators of the Web. We believe this act of human collaboration across an open platform is essential to individual growth and our collective future.",
    b6:"Read the",
    b7:"Mozilla Manifesto",
    b8:"to learn even more about the values and principles that guide the pursuit of our mission.",
    imie:"Enter your name",
  }
};
class translator {
  constructor() {
    this.changeLanguage();
  }

  changeLanguage() {
    let tl = tlumaczenie;
    $(".language select").change(function() {
      let lang = $(this).val();
      $("#a1").text(tl[lang].b1, 500);
      $("#a2").text(tl[lang].b2, 500);
      $("#a3").text(tl[lang].b3, 500);
      $("#a4").text(tl[lang].b4, 500);
      $("#a5").text(tl[lang].b5, 500);
      $("#a6").text(tl[lang].b6, 500);
      $("#a7").text(tl[lang].b7, 500);
      $("#a8").text(tl[lang].b8, 500);
      $("#imie").text(tl[lang].imie, 500);
    });
  }
}
new translator()

  /*W Mozilli jesteśmy globalną społecznością

  technolodzy
  myśliciele
  tworzyciele

  współpracując, aby Internet był żywy i dostępny, aby ludzie na całym świecie mogli być informowanymi współtwórcami i twórcami sieci. Wierzymy, że ten akt ludzkiej współpracy na otwartej platformie ma zasadnicze znaczenie dla indywidualnego rozwoju i naszej zbiorowej przyszłości.

Przeczytaj Manifest Mozilli, aby dowiedzieć się jeszcze więcej o wartościach i zasadach, które kierują naszą misją.*/